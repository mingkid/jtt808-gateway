package resp

type Term struct {
	SIM          string  // SIM 卡号
	Lng          float64 // 经度
	Lat          float64 // 纬度
	LocateAt     int64   // 定位时间
	IsOnline     bool    // 连接状态: 在线
	IsToRegister bool    // 连接状态: 注册中
	IsToAuth     bool    // 连接状态: 鉴权中
	IsOffline    bool    // 连接状态: 离线
}
